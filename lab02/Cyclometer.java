//David Ruiz
//CSE002
//Professor Chen
//This program will emulate a cyclometer, taking various variables as inputs, like time and cycles, and output calculated values, such as distance

public class Cyclometer {
    
    //Main method required for every Java program
    public static void main(String[] args) {
        
        //Variables
        int secsTrip1=480; //Time elapsed of trip 1
        int secsTrip2=3220; //Time elapsed of trip 2
        int countsTrip1=1561; //Number of rotations from trip 1
        int countsTrip2=9037; //Number of rotations from trip 2
        double wheelDiameter=27.0,  //Diameter of wheel 
        PI=3.14159, //Pi intialized
        feetPerMile=5280, //Conversion from mile to feet
        inchesPerFoot=12, //Conersion from foot to inches
        secondsPerMinute=60; //Conversion from minute to seconds
        double distanceTrip1, distanceTrip2,totalDistance; //INitalized the variables for the respective trip distances and the total distance

        //Print variables through terminal 
        System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
        System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");

        //Calculations
        distanceTrip1=countsTrip1*wheelDiameter*PI;
        // Above gives distance in inches
        //(for each count, a rotation of the wheel travels
        //the diameter in inches times PI)
        distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
        distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //Calculates distance for trip 2
        totalDistance=distanceTrip1+distanceTrip2; //Calculates total distance

        //Print out calculations
        //Print out the output data.
        System.out.println("Trip 1 was "+distanceTrip1+" miles"); //Prints the distance of trip 1
        System.out.println("Trip 2 was "+distanceTrip2+" miles"); //Prints the distance of trip 2
        System.out.println("The total distance was "+totalDistance+" miles"); //Prints the total distance

        
    } //End of main method
    
} // End of class