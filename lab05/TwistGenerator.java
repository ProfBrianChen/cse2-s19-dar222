//David Ruiz
//CSE002
//Professor Chen
//This program will print out a twist like image using loops

import java.util.Scanner;

public class TwistGenerator { //Beginning of the main class

    public static void main(String[] args) { //Beginning of the main method
        //Scanner initialization
        Scanner scanner = new Scanner(System.in);
        
        //Variables
        int length;
        
        //Terminal user interaction
        do{
            System.out.println("Enter a postive integer: ");
            while(!scanner.hasNextInt()){
                System.out.println("Please enter an integer only: ");
                scanner.next();
            }
            length = scanner.nextInt();
        }while(length < 1);
      
        //Twist output
        String line = "";
        
        int outer = 3;
        int inner = 1;
        
        do {
            do {
                if(outer == 3){
                    if(inner % 3 == 1){
                        line += "\\";
                    }
                    if(inner % 3 == 2){
                       line += " ";
                    }
                    if(inner % 3 == 0){
                        line += "/";
                    }
                }
                if(outer == 2){
                    if(inner % 3 == 1){
                        line += " ";
                    }
                    if(inner % 3 == 2){
                       line += "x";
                    }
                    if(inner % 3 == 0){
                        line += " ";
                    }
                }
                if (outer == 1){
                    if(inner % 3 == 1){
                        line += "/";
                    }
                    if(inner % 3 == 2){
                       line += " ";
                    }
                    if(inner % 3 == 0){
                        line += "\\";
                    }
                }
                inner += 1;
            } while(inner <= length);
            System.out.print(line);
            System.out.println();
            line = "";
            inner = 1;
            outer -= 1;
        } while(outer >= 1);
        
    } //End of the main method
    
}//End of the main class
