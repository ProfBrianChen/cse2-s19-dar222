import java.util.Scanner;

public class BoxVolume { //Start of main method

    public static void main(String[] args) { //Start of main method
        //Initilializing scanner
        Scanner scanner = new Scanner(System.in);
        
        //Variables
        double length;
        double width;
        double height;
        double volume;
        
        //User-terminal interaction
        System.out.print("The length of the side of the box is: ");
        length = scanner.nextDouble();
        System.out.print("The width of the side of the box is: ");
        width = scanner.nextDouble();
        System.out.print("The height of the side of the box is: ");
        height = scanner.nextDouble();
        
        //Calculations
        volume = length * width * height;
        
        //Final output
        System.out.println("The volume inside the box is: " + volume);
        
        
    }//End of main method
    
}//End of class