import java.util.Scanner;

public class Convert { //Start of class

    public static void main(String[] args) { //Start of main method
        //Initializing scanner
        Scanner scanner = new Scanner(System.in);
        
        //Variables
        double meters;
        double inches;
        
        //User-terminal interaction
        System.out.print("Enter the distance in meters: ");
        meters = scanner.nextDouble();
        
        //Calculations
        inches = meters * 39.3701;
        inches = Math.floor(inches * 100) / 100;
        //Final print
        System.out.println(meters + " meters is " + inches + " inches.");
        
    }//End of main method
    
}//End of class
