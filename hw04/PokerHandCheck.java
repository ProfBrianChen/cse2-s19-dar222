//David Ruiz
//CSE002
//Professor Chen
//The purpose of this assignment is to pose as a mechanism that chooses five random cards from five respective decks and then analyzes whether the cards drawn resemble any pattern in typical card game convention

public class PokerHandCheck {

    public static void main(String[] args) {
        //Variables
        int cardone;
        int cardtwo;
        int cardthree;
        int cardfour;
        int cardfive;
        
        String suit = "";
        String identity = "";
        
        String onename;
        String twoname;
        String threename;
        String fourname;
        String fivename;
        
        //Random card generation 
        cardone = (int)(Math.random() * 52 + 1);
        cardtwo = (int)(Math.random() * 52 + 1);
        cardthree = (int)(Math.random() * 52 + 1);
        cardfour = (int)(Math.random() * 52 + 1);
        cardfive = (int)(Math.random() * 52 + 1);
        
        //Card one
        //Suit categorization
        if (cardone > 0 && cardone < 14){
            suit = "Clubs";
        }
        if (cardone > 13 && cardone < 27){
            suit = "Diamonds";
        }
        if (cardone > 26 && cardone < 40){
            suit = "Hearts";
        }
        if (cardone > 39 && cardone < 53){
            suit = "Spades";
        }
        
        //Identity equalization
        if ("Diamonds".equals(suit)){
            cardone -= 13;
        }
        if ("Hearts".equals(suit)){
            cardone -= 26;
        }
        if ("Spades".equals(suit)){
            cardone -= 39;
        }
        
        //Identity categorization
        switch (cardone){
            case 1: identity = "Ace";
                    break;
            case 2: identity = "2";
                    break;
            case 3: identity = "3";
                    break;
            case 4: identity = "4";
                    break;
            case 5: identity = "5";
                    break;
            case 6: identity = "6";
                    break;
            case 7: identity = "7";
                    break;
            case 8: identity = "8";
                    break;
            case 9: identity = "9";
                    break;
            case 10: identity = "10";
                    break;
            case 11: identity = "Jack";
                    break;
            case 12: identity = "Queen";
                    break;
            case 13: identity = "King";
                    break;         
        }
        
        onename = identity + " of " + suit;
        
        //Card two
        //Suit categorization
        if (cardtwo > 0 && cardtwo < 14){
            suit = "Clubs";
        }
        if (cardtwo > 13 && cardtwo < 27){
            suit = "Diamonds";
        }
        if (cardtwo > 26 && cardtwo < 40){
            suit = "Hearts";
        }
        if (cardtwo > 39 && cardtwo < 53){
            suit = "Spades";
        }
        
        //Identity equalization
        if ("Diamonds".equals(suit)){
            cardtwo -= 13;
        }
        if ("Hearts".equals(suit)){
            cardtwo -= 26;
        }
        if ("Spades".equals(suit)){
            cardtwo -= 39;
        }
        
        //Identity categorization
        switch (cardtwo){
            case 1: identity = "Ace";
                    break;
            case 2: identity = "2";
                    break;
            case 3: identity = "3";
                    break;
            case 4: identity = "4";
                    break;
            case 5: identity = "5";
                    break;
            case 6: identity = "6";
                    break;
            case 7: identity = "7";
                    break;
            case 8: identity = "8";
                    break;
            case 9: identity = "9";
                    break;
            case 10: identity = "10";
                    break;
            case 11: identity = "Jack";
                    break;
            case 12: identity = "Queen";
                    break;
            case 13: identity = "King";
                    break;         
        }
        
        twoname = identity + " of " + suit;
        
        //Card three
        //Suit categorization
        if (cardthree > 0 && cardthree < 14){
            suit = "Clubs";
        }
        if (cardthree > 13 && cardthree < 27){
            suit = "Diamonds";
        }
        if (cardthree > 26 && cardthree < 40){
            suit = "Hearts";
        }
        if (cardthree > 39 && cardthree < 53){
            suit = "Spades";
        }
        
        //Identity equalization
        if ("Diamonds".equals(suit)){
            cardthree -= 13;
        }
        if ("Hearts".equals(suit)){
            cardthree -= 26;
        }
        if ("Spades".equals(suit)){
            cardthree -= 39;
        }
        
        //Identity categorization
        switch (cardthree){
            case 1: identity = "Ace";
                    break;
            case 2: identity = "2";
                    break;
            case 3: identity = "3";
                    break;
            case 4: identity = "4";
                    break;
            case 5: identity = "5";
                    break;
            case 6: identity = "6";
                    break;
            case 7: identity = "7";
                    break;
            case 8: identity = "8";
                    break;
            case 9: identity = "9";
                    break;
            case 10: identity = "10";
                    break;
            case 11: identity = "Jack";
                    break;
            case 12: identity = "Queen";
                    break;
            case 13: identity = "King";
                    break;         
        }
        
        threename = identity + " of " + suit;
        
        //Card four
        //Suit categorization
        if (cardfour > 0 && cardfour < 14){
            suit = "Clubs";
        }
        if (cardfour > 13 && cardfour < 27){
            suit = "Diamonds";
        }
        if (cardfour > 26 && cardfour < 40){
            suit = "Hearts";
        }
        if (cardfour > 39 && cardfour < 53){
            suit = "Spades";
        }
        
        //Identity equalization
        if ("Diamonds".equals(suit)){
            cardfour -= 13;
        }
        if ("Hearts".equals(suit)){
            cardfour -= 26;
        }
        if ("Spades".equals(suit)){
            cardfour -= 39;
        }
        
        //Identity categorization
        switch (cardfour){
            case 1: identity = "Ace";
                    break;
            case 2: identity = "2";
                    break;
            case 3: identity = "3";
                    break;
            case 4: identity = "4";
                    break;
            case 5: identity = "5";
                    break;
            case 6: identity = "6";
                    break;
            case 7: identity = "7";
                    break;
            case 8: identity = "8";
                    break;
            case 9: identity = "9";
                    break;
            case 10: identity = "10";
                    break;
            case 11: identity = "Jack";
                    break;
            case 12: identity = "Queen";
                    break;
            case 13: identity = "King";
                    break;         
        }
        
        fourname = identity + " of " + suit;
        
        //Card five
        //Suit categorization
        if (cardfive > 0 && cardfive < 14){
            suit = "Clubs";
        }
        if (cardfive > 13 && cardfive < 27){
            suit = "Diamonds";
        }
        if (cardfive > 26 && cardfive < 40){
            suit = "Hearts";
        }
        if (cardfive > 39 && cardfive < 53){
            suit = "Spades";
        }
        
        //Identity equalization
        if ("Diamonds".equals(suit)){
            cardfive -= 13;
        }
        if ("Hearts".equals(suit)){
            cardfive -= 26;
        }
        if ("Spades".equals(suit)){
            cardfive -= 39;
        }
        
        //Identity categorization
        switch (cardfive){
            case 1: identity = "Ace";
                    break;
            case 2: identity = "2";
                    break;
            case 3: identity = "3";
                    break;
            case 4: identity = "4";
                    break;
            case 5: identity = "5";
                    break;
            case 6: identity = "6";
                    break;
            case 7: identity = "7";
                    break;
            case 8: identity = "8";
                    break;
            case 9: identity = "9";
                    break;
            case 10: identity = "10";
                    break;
            case 11: identity = "Jack";
                    break;
            case 12: identity = "Queen";
                    break;
            case 13: identity = "King";
                    break;         
        }
        
        fivename = identity + " of " + suit;
        
        //Hand combination determination
        String combo = "high card hand";
        
        if((cardone == cardtwo) || (cardone == cardthree) || (cardone == cardfour) || (cardone == cardfive) || (cardtwo == cardthree) || (cardtwo == cardfour) || (cardtwo == cardfive) || (cardthree == cardfour) || (cardthree == cardfive) || (cardfour == cardfive)){
            combo = "pair";
        }
        
        if((cardone == cardtwo && cardthree == cardfour) || (cardone == cardtwo && cardfour == cardfive) || (cardone == cardtwo && cardthree == cardfive) || (cardtwo == cardthree && cardfour == cardfive) || (cardtwo == cardthree && cardfive == cardone) || (cardtwo == cardthree && cardfour == cardone) || (cardthree == cardfour && cardfive == cardone) || (cardthree == cardfour && cardfive == cardtwo) || (cardfour == cardfive && cardone == cardthree) || (cardfive == cardone && cardtwo == cardfour) || (cardone == cardthree && cardtwo == cardfour) || (cardone == cardthree && cardtwo == cardfive) || (cardone == cardfour && cardtwo == cardfive) || (cardone == cardfour && cardthree == cardfive) || (cardtwo == cardfour && cardthree == cardfive)){
            combo = "double-pair";
        }
        
        if((cardone == cardtwo && cardone == cardthree) || (cardone == cardtwo && cardone == cardfour) || (cardone == cardtwo && cardone == cardfive) || (cardone == cardthree && cardone == cardfour) || (cardone == cardthree && cardone == cardfive) || (cardone == cardfour && cardone == cardfive) || (cardtwo == cardthree && cardtwo == cardfour) || (cardtwo == cardthree && cardtwo == cardfive) || (cardthree == cardfour && cardthree == cardfive)){
            combo = "three of a kind";
        }
        
        //Final output
        System.out.println("Your random cards were: ");
        System.out.println(onename);
        System.out.println(twoname);
        System.out.println(threename);
        System.out.println(fourname);
        System.out.println(fivename);
        System.out.println("You got a " + combo + "!");
        
    }
    
}
