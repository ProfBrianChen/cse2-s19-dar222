//David Ruiz
//CSE002
//Professor Chen
//This program will use the Math class in order to randomly generate numbers and translate that into a card picking scenario

public class CardGenerator {

    public static void main(String[] args) {
        //Variables
        int randnum;
        String suit = "";
        String identity = "";
        String card = "";
        
        //Random Number Generator
        randnum = (int)(Math.random() * 51 + 1);
        
        //Suit categorization
        if (randnum > 0 && randnum < 14){
            suit = "Clubs";
        }
        if (randnum > 13 && randnum < 27){
            suit = "Diamonds";
        }
        if (randnum > 26 && randnum < 40){
            suit = "Hearts";
        }
        if (randnum > 39 && randnum < 53){
            suit = "Spades";
        }
        
        //Identity equalization
        if ("Diamonds".equals(suit)){
            randnum -= 13;
        }
        if ("Hearts".equals(suit)){
            randnum -= 26;
        }
        if ("Spades".equals(suit)){
            randnum -= 39;
        }
        
        //Identity categorization
        switch (randnum){
            case 1: identity = "Ace";
                    break;
            case 2: identity = "2";
                    break;
            case 3: identity = "3";
                    break;
            case 4: identity = "4";
                    break;
            case 5: identity = "5";
                    break;
            case 6: identity = "6";
                    break;
            case 7: identity = "7";
                    break;
            case 8: identity = "8";
                    break;
            case 9: identity = "9";
                    break;
            case 10: identity = "10";
                    break;
            case 11: identity = "Jack";
                    break;
            case 12: identity = "Queen";
                    break;
            case 13: identity = "King";
                    break;         
        }
        
        //Card returned
        System.out.println("You picked the " + identity + " of " + suit);
        
    }
    
}