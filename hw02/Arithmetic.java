//David Ruiz
//CSE002
//Professor Chen
//This program will take in variables such as cost and amount in order to calculate the price of certain items, the relevant tax, and total cost of these items with and without tax

public class Arithmetic {

    public static void main(String[] args) {
        //Variables
        int numPants = 3; //Number of pairs of pants
        double pantPrice = 34.98; //Cost per pair of pants
        int numShirts = 2; //Number of sweatshirts
        double shirtPrice = 24.99; //Cost per shirt
        int numBelts = 1; //Number of belts
        double beltCost = 33.99; //Cost per belt
        double paSalesTax = 0.06; //Pennsylvania taax rate
        
        //Calculations
        //Initializing variables
        double totalPants;
        double totalShirts;
        double totalBelts;
        
        totalPants = numPants * pantPrice;
        totalShirts = numShirts * shirtPrice;
        totalBelts = numBelts * beltCost;
        
        double pantTax;
        double shirtTax;
        double beltTax;
        
        pantTax = Math.floor((totalPants * paSalesTax) * 100) / 100;
        shirtTax = Math.floor((totalShirts * paSalesTax) * 100) / 100;
        beltTax = Math.floor((totalBelts * paSalesTax) * 100) / 100;
        
        double totalCost;
        
        totalCost = totalPants + totalShirts + totalBelts;
        
        double totalTax;
        
        totalTax = Math.floor((totalCost * paSalesTax) * 100) / 100;
        
        double finalCost;
        
        finalCost = Math.floor((totalCost - totalTax) * 100) / 100;
        
        //Display values after calculated
        System.out.println("The total cost of pants is $" + totalPants + ", and the total amount of tax on the pants is $" + pantTax);
        System.out.println("The total cost of shirts is $" + totalShirts + ", and the total amount of tax on the shirts is $" + shirtTax);
        System.out.println("The total cost of belts is $" + totalBelts + ", and the total amount of tax on the belts is $" + beltTax);
        System.out.println("The total cost of the purchases before tax is $" + totalCost);
        System.out.println("The total amount of tax on the purchases is $" + totalTax);
        System.out.println("The total cost of the purchases after tax is $" + finalCost);
        
    }
    
}