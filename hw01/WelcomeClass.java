//David Ruiz
//CSE002
//Professor Chen
//This program will output the user's student ID surrounded by text art. A short statement will also be displayed. 

public class WelcomeClass {

    public static void main(String[] args) {
        System.out.println("-----------");
        System.out.println("| WELCOME |");
        System.out.println("-----------");
        System.out.println("  ^  ^  ^  ^  ^  ^");
        System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
        System.out.println("<-D--A--R--2--2--2->");
        System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
        System.out.println("  v  v  v  v  v  v");
        System.out.println("Hi, my name is David Ruiz. I am a first year student attending Lehigh University planning to major in Computer Science.");
        
    }
    
}